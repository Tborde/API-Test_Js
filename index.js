require('dotenv').config()

// IMPORT
import Express from 'express'
import BodyParser from 'body-parser'
import router from './routes/api'
import mongoose from 'mongoose'

// CONST
const app = Express()

// APP USE
app.use(BodyParser.json())
app.use(BodyParser.urlencoded({ extended: true }))
app.use('/api', router)

// MONGO
mongoose.Promise = global.Promise
mongoose.connect(`mongodb://${process.env.ENV_HOST}/${process.env.ENV_DATABASE}`)

// SERVER RUNNING
app.listen(process.env.SERVER_PORT, function() {
    console.log(
        `Server running at port ${process.env.SERVER_PORT}: 
            http://${process.env.SERVER_HOST}:${process.env.SERVER_PORT}/api
            http://localhost:${process.env.SERVER_PORT}/api`,
    )
})
