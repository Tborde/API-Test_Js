import mongoose from 'mongoose'

const User = new mongoose.Schema({
    lastname: String,
    firstname: String,
    email: String,
    pseudo: String,
    password: String,
})

export default mongoose.model('User', User)
